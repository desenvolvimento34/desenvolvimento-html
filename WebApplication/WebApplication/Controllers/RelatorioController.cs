﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebApplication.Models;

namespace WebApplication.Controllers
{
    public class RelatorioController : Controller
    {
        // GET: Relatorio
        public ActionResult Relatorios()
        {
            return View(Models.Relatorios.ListaRelatorios());
        }

        public ActionResult Consultar(int id)
        {
            Relatorios r = Models.Relatorios.Consulta(id);
            if (r == null)
            {
                TempData["Msg"] = "Erro ao buscar produto!";
                return RedirectToAction("Relatorios");
            }
            return View(r);
        }


    }
}
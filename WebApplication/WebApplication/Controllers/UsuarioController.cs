﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebApplication.Models;

namespace WebApplication.Controllers
{
    public class UsuarioController : Controller
    {
        // GET: Usuario
        public ActionResult Usuario() //Atribuindo View Login
        {
            return View();  //Conexão com a view login, onde o mesmo terá a função de lgoar com o usuário.
        }
      
        [HttpPost]

        public ActionResult Login(string cpf, string senha) //Método de Login
        {
            Usuario user = new Usuario();  // Instanciando usuário
            user.CPF = cpf; //Pegando método do cpf e igualando com variavel do metodo para o acesso do sistema;
            user.SENHA = senha; // Pegando método do banco de dados e igualando com a variavel do método de acesso;

            if (user.Login()) //Banco de dados - método de login
            {
                Session["User"] = user;
                return RedirectToAction("Principal", "Home"); //Retorna a view para fazer o login o sistema
            }
            else
            {
                return RedirectToAction("Usuario", "Usuario"); // Retornar view;
            }
             
        }

        public ActionResult Sair () //Método sair
        {
            Session["User"] = null;
            return RedirectToAction("Usuario", "Usuario"); //Retorna view login
        }

    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebApplication.Models;

namespace WebApplication.Controllers
{
    public class CadastroController : Controller
    {
      
        // GET: Cadastro
        public ActionResult Cadastro()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Cadastrar(string nome, string cpf, string senha, string tipoUsuario)
        {
            Cadastro cad = new Cadastro();
            cad.Nome = nome;
            cad.Cpf = cpf;
            cad.Senha = senha;
            cad.Tipousuario = tipoUsuario;

            TempData["Msg"] = cad.Inserir();


            //Retornando

            return RedirectToAction("Cadastro", "Cadastro"); // Retornar view;

        }
    }
}
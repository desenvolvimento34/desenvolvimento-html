﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebApplication.Models;

namespace WebApplication.Controllers
{
    public class AnotacoesController : Controller
    {
        // GET: Anotacoes
        public ActionResult Anotacoes()
        {
            Usuario user = (Usuario)Session["User"];
     
            return View();
        }

    
        //Método de Inserir (anotar)

        [HttpPost]
        public ActionResult Inserir (string descricao, string titulo)
        {
            
            Usuario user = (Usuario)Session["User"];

            Anotacoes an = new Anotacoes();
            an.IdPessoa = user.IdPessoa;
            an.Descricao = descricao;
            an.Titulo = titulo;

            TempData["Msg"] = an.InserirAn();
            //Retornando

            return RedirectToAction("Anotacoes", "Anotacoes"); // Retornar view;
        }

        //Método de deletar anotações
        public ActionResult Deletar(int codigoA)
        {
            Anotacoes an = new Anotacoes();
            an.CodigoAnotacoes = codigoA;

            TempData["Msg"] = an.Remover();

            //Retornando
            return RedirectToAction("Anotacoes", "Anotacoes"); //Retornar view;
        }

        //Método de editar anotações
        [HttpPost]
        public ActionResult Editar(int idpessoa, string descricao, string titulo)
        {
            Anotacoes an = new Anotacoes();
            an.IdPessoa = idpessoa;
            an.Descricao = descricao;
            an.Titulo = titulo;

            string mens = an.Editar();
            TempData["Msg"] = mens;
            if (mens == "Salvo com sucesso!")
            {
                //Retornando
                return RedirectToAction("Anotacoes", "Anotacoes"); //Retornar view;
            }
            else
            {

                return View();
            }

        }
    }
}
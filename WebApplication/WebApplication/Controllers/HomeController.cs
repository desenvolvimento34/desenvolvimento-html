﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebApplication.Models;

namespace WebApplication.Controllers
{
    public class HomeController : Controller
    {
        //Controller da Dashboard
        // GET: Home
        public ActionResult Principal() //Atribuindo View Dash
        {
            return View();
        }

        [HttpPost]

        public ActionResult Mostrar (double mv_valvula, double sp_pressao, double pv_pressao, double pv_temperatura)
        {
            Dashboard dash = new Dashboard();
            dash.MV_Valvula = mv_valvula;
            dash.SP_Pressao = sp_pressao;
            dash.PV_Pressao = pv_pressao;
            dash.PV_Temperatura = pv_temperatura;

            TempData["Msg"] = dash.Mostrar();
            //Retornando

            return RedirectToAction("Home", "Home"); // Retornar view;
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace WebApplication.Models
{
    public class Relatorios
    {
        private int codigoplanta;
        private string datahora, mvValvula, spPressao, pvPressao, pvTemperatura;

        public string DataHora // Método data
        {
            get//pegando dados privados
            {
                return datahora;
            }

            set//editando
            {
                datahora = value;
            }
        }

      
        public string MvValvula //metodo valvula
        {
            get//pegando dados privados
            {
                return mvValvula;

            }
            set//editando
            {
                mvValvula = value;
            }
        }

        public string SpPressao //metodo SPpressao
        {
            get //pegando dados privados
            {
                return spPressao;
            }
            set//editando
            {
                spPressao = value;
            }
        }

        public string PvPressao //metodo PVPressao
        {
            get//pegando dados privados
            {
                return pvPressao;
            }
            set//editando 
            {
                pvPressao = value;
            }
        }

        public string PvTemperatura //metodo PVTemperatura
        {
            get //pegar dados privados
            {
                return pvTemperatura;
            }
            set//editando dados
            {
                pvTemperatura = value;
            }
        }
        public int CodigoPlanta //Método 
        {
            get //Pegando os dados privados
            {
                return codigoplanta; //retornando variavel 
            }

            set //Muda os dados privados para serem editados
            {
                codigoplanta = value; //valor do 
            }
        }

        //Método de listagem de dados (Listar dados da planta);
        public static List<Relatorios> ListaRelatorios()
        {
            List<Relatorios> list = new List<Relatorios>(); //Instanciando Listagem 
            //Chamando conexão com o banco de dados
            SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["BCD"].ConnectionString);
            try
            {
                con.Open(); //Abrindo conexão
                SqlCommand query = new SqlCommand("SELECT Planta from data_hora, mv_valvula, SP_Pressao, PV_Pressao. PV-Temperatura WHERE CAST(FLOOR(CAST(DATA AS float))AS datetime)>=@data_hora AND CAST(FLOOR(CAST(DATA AS float))AS datetime)<=@data_hora", con);
                SqlDataReader leitor = query.ExecuteReader();

                while (leitor.Read())
                {
                    Relatorios r = new Relatorios();
                    r.DataHora = leitor["data_hora"].ToString();
                    r.MvValvula = leitor["mv_valvula"].ToString();
                    r.SpPressao = leitor["SP_Pressao"].ToString();
                    r.PvPressao = leitor["PV_Pressao"].ToString();
                    r.PvTemperatura = leitor["PV_Temperatura"].ToString();

                    list.Add(r);//Adicionar lista 
                }
            }catch(Exception e)
            {
                list = new List<Relatorios>();
            }

            if (con.State == System.Data.ConnectionState.Open)
                con.Close();

            return list; //Retorna LISTA

        }

        //Método de Consultar dados (Inserir os dados que o usuário (administrador) deseja;

        public static Relatorios Consulta(int codigoPlanta)
        {
            Relatorios r = new Relatorios();
            //Chamando conexão com o banco de dados
            SqlConnection con =
            new SqlConnection(ConfigurationManager.ConnectionStrings["BCD"].ConnectionString);

            try
            {
                con.Open(); //Abrindo Conexão com o banco de dados
                SqlCommand query = new SqlCommand("SELECT Planta from data_hora, mv_Valvula, SP_Pressao, PV_Pressao, PV_Temperatura WHERE id_NumRegistro = @id_NumRegistro"
                    , con);
                query.Parameters.AddWithValue("@id_NumRegistro", codigoPlanta);
                SqlDataReader leitor = query.ExecuteReader();

                while (leitor.Read()){
                    r.CodigoPlanta = int.Parse(leitor["id_NumRegistro"].ToString());
                    r.DataHora = leitor["data_hora"].ToString();
                    r.MvValvula = leitor["mv_valvula"].ToString();
                    r.SpPressao = leitor["SP_Pressao"].ToString();
                    r.PvPressao = leitor["PV_Pressao"].ToString();
                    r.PvTemperatura = leitor["PV_Temperatura"].ToString();

                }
            }
            catch (Exception e)
            {
                r = null;
            }

            if (con.State == System.Data.ConnectionState.Open)
                con.Close();

            return r; //Retorna CONSULTA
        }


    }
    }

﻿using System;
using System.Threading.Tasks;
using Microsoft.Owin;
using Owin;
using System.Data.SqlClient;
using System.Configuration;

[assembly: OwinStartup(typeof(WebApplication.Models.Usuario))]

namespace WebApplication.Models
{
    public class Usuario
    {
        private string cpf, senha, tipoUsuario; //Variaveis para o acesso do sistema
        private int idpessoa;

        public string CPF //Método CPF
        {
            get //Pegando os dados privados
            {
                return cpf; //retornando variavel cpf
            }

            set //Muda os dados privados para serem editados
            {
                cpf = value; //valor do cpf
            }
        }

        public string SENHA //Método acesso
        {
            get //Pegando os dados privados
            {
                return senha; //retornando variavel senha
            }

            set //Muda os dados privados para serem editados
            {
                senha = value; //valor do senha
            }
        }

        public string TipoUsuario //Método tipo usuario
        {
            get //Pegando os dados privados
            {
                return tipoUsuario; //retornando variavel tipoUsuario
            }

            set //Muda os dados privados para serem editados
            {
                tipoUsuario = value; //valor do tipoUsuario
            }
        }
        public int IdPessoa //Método tipo usuario
        {
            get //Pegando os dados privados
            {
                return idpessoa; //retornando variavel tipoUsuario
            }

            set //Muda os dados privados para serem editados
            {
                idpessoa = value; //valor do tipoUsuario
            }
        }

        public bool Login() // Método Login ao sistema com bando de dados
        {
            bool login = false; // valor boleano
            SqlConnection con =
                 new SqlConnection(ConfigurationManager.ConnectionStrings["BCD"].ConnectionString);
            try
            {
                con.Open();
                SqlCommand query =
                    new SqlCommand("SELECT * FROM Pessoa WHERE cpf = @cpf AND senha = @senha ;", con);
                query.Parameters.AddWithValue("@cpf", this.cpf); //Banco de dados, conexão com o atibuto cpf;
                query.Parameters.AddWithValue("@senha", this.senha); // Bando de dados, atitbuto senha;
                SqlDataReader leitor = query.ExecuteReader();


                while (leitor.Read())
                {
                    this.TipoUsuario = leitor["tipo_usuario"].ToString();
                    this.IdPessoa = int.Parse(leitor["id_pessoa"].ToString());
                }

                login = leitor.HasRows;
            }
            catch (Exception e)
            {
                login = false;
            }

            if (con.State == System.Data.ConnectionState.Open)
                con.Close();

            return login; //Retorna váriavel login

        }


    }
}


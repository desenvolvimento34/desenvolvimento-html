﻿using Microsoft.Owin;
using Owin;
using System;
using System.Configuration;
using System.Data.SqlClient;
using System.Runtime.Remoting.Contexts;
using System.Threading.Tasks;
using System.Web.Script.Serialization;

[assembly: OwinStartup(typeof(WebApplication.Models.Cadastro))]

namespace WebApplication.Models
{
    public class Cadastro
    {
        private string nome, cpf, senha, tipoUsuario;

        public string Nome //Método 
        {
            get //Pegando os dados privados
            {
                return nome; //retornando variavel 
            }

            set //Muda os dados privados para serem editados
            {
                nome = value; //valor do 
            }
        }


        public string Cpf //Método CPF
        {
            get //Pegando os dados privados
            {
                return cpf; //retornando variavel cpf
            }

            set //Muda os dados privados para serem editados
            {
                cpf = value; //valor do cpf
            }
        }

        public string Senha //Método acesso
        {
            get //Pegando os dados privados
            {
                return senha; //retornando variavel senha
            }

            set //Muda os dados privados para serem editados
            {
                senha = value; //valor do senha
            }
        }

        public string Tipousuario //Método tipo usuario
        {
            get //Pegando os dados privados
            {
                return tipoUsuario; //retornando variavel tipoUsuario
            }

            set //Muda os dados privados para serem editados
            {
                tipoUsuario = value; //valor do tipoUsuario
            }
        }

       public string Inserir() { 
            string ins = "Inserido com sucesso!";

            SqlConnection con =
               new SqlConnection(ConfigurationManager.ConnectionStrings["BCD"].ConnectionString);
            try
            {
                con.Open();
                SqlCommand query =
                    new SqlCommand("INSERT INTO Pessoa VALUES ( @nome, @cpf, @senha, @tipo_usuario );", con);
          
                query.Parameters.AddWithValue("@nome", Nome);
                query.Parameters.AddWithValue("@cpf", Cpf); //Banco de dados, conexão com o atibuto;
                query.Parameters.AddWithValue("@senha", Senha); // Banco de dados, atitbuto;
                query.Parameters.AddWithValue("@tipo_usuario", tipoUsuario); //Bnaoc de dados, conexão atributo

              

                if(query.ExecuteNonQuery() == 0)
                {
                    ins = "Erro ao inserir!";
                }

            }
            catch (Exception e)
            {

                ins = e.Message;
                

            }

                return ins;

        }
    }
}


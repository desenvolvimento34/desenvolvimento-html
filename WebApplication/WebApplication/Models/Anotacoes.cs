﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Microsoft.AspNet.SignalR;
using System.Data.SqlClient;
using System.Configuration;
using System.Data;

namespace WebApplication.Models
{
    public class Anotacoes
    {
        private string titulo, descricao, datahora;
        private int codigoAnotacoes, idpessoa;


        public string Titulo //Método 
        {
            get //Pegando os dados privados
            {
                return titulo; //retornando variavel 
            }

            set //Muda os dados privados para serem editados
            {
                titulo = value; //valor do 
            }
        }

        public string Descricao //Método 
        {
            get //Pegando os dados privados
            {
                return descricao; //retornando variavel 
            }

            set //Muda os dados privados para serem editados
            {
                descricao = value; //valor do 
            }
        }

        public string DataHora //Método 
        {
            get //Pegando os dados privados
            {
                return datahora; //retornando variavel 
            }

            set //Muda os dados privados para serem editados
            {
                datahora = value; //valor do 
            }
        }

        public int CodigoAnotacoes //Método 
        {
            get //Pegando os dados privados
            {
                return codigoAnotacoes; //retornando variavel 
            }

            set //Muda os dados privados para serem editados
            {
                codigoAnotacoes = value; //valor do 
            }
        }
        public int IdPessoa //Método 
        {
            get //Pegando os dados privados
            {
                return idpessoa; //retornando variavel 
            }

            set //Muda os dados privados para serem editados
            {
                idpessoa = value; //valor do 
            }
        }

        //Metodo Inserir
        public string InserirAn()
        {
            string mensa = "Dados inseridos com sucesso";


            SqlConnection con =
               new SqlConnection(ConfigurationManager.ConnectionStrings["BCD"].ConnectionString);
            try
            {
                con.Open();
                SqlCommand query =
                    new SqlCommand("INSERT INTO Anotacoes (fk_id_pessoa,titulo, descricao, datahora) VALUES (@id_pessoa,@titulo,@descricao,GETDATE());", con);
                query.Parameters.AddWithValue("@id_pessoa", idpessoa);
                query.Parameters.AddWithValue("@titulo", titulo);
                query.Parameters.AddWithValue("@descricao", descricao);

                if (query.ExecuteNonQuery() == 0)
                {
                    mensa = "Erro ao inserir!";
                }

            }
            catch (Exception e)
            {

                mensa = e.Message;


            }

            return mensa;

        }


        //Métdo Editar
        public string Editar()
        {
            string mens = "Editado com sucesso";

            SqlConnection con =
                new SqlConnection(ConfigurationManager.ConnectionStrings["BDC"].ConnectionString);
            try
            {
                con.Open();
                SqlCommand query =
                    new SqlCommand("UPDATE Anotacoes SET titulo = @titulo, descricao = @descricao,datahora = GETDATE() WHERE codigoAnotacoes = @id_anotacoes );", con);
                //passagem dos parametros - puxando do banco de dados
                query.Parameters.AddWithValue("@id_anotacoes", codigoAnotacoes);
                query.Parameters.AddWithValue("@titulo", titulo);
                query.Parameters.AddWithValue("@descricao", descricao);

                query.ExecuteNonQuery();

            }
            catch (Exception e)
            {
                mens = e.Message;
            }

            if (con.State == System.Data.ConnectionState.Open)
                con.Close();

            return mens;

        }


        //Metodo remover anotações
        public string Remover()
        {
            string mensagem = "Removido com sucesso";

            SqlConnection con =
              new SqlConnection(ConfigurationManager.ConnectionStrings["BCD"].ConnectionString);
            try
            {
                con.Open();
                SqlCommand query =
                    new SqlCommand("DELETE FROM Anotacoes WHERE codigo = @id_anaotacoes);", con);
                //passagem dos parametros - puxando do banco de dados
                query.Parameters.AddWithValue("@id_anotacoes", codigoAnotacoes); //Banco de dados, conexão com o atributo codigo;


                query.ExecuteNonQuery();

            }
            catch (Exception e)
            {
                mensagem = e.Message;
            }

            if (con.State == ConnectionState.Open)
                con.Close();

            return mensagem;

        }

        //Listagem das Anotaçoes
        public static List<Anotacoes> ListaAnotacoes(int idpessoa)
        {

            List<Anotacoes> list = new List<Anotacoes>(); //Instanciando listagem
            SqlConnection con =
            new SqlConnection(ConfigurationManager.ConnectionStrings["BCD"].ConnectionString);
            try
            {
                con.Open();
                SqlCommand query = new SqlCommand("SELECT * from Anotacoes WHERE fk_id_pessoa = @id_pessoa", con);
                query.Parameters.AddWithValue("@id_pessoa", idpessoa);
                SqlDataReader leitor = query.ExecuteReader();

                while (leitor.Read())
                {
                    Anotacoes a = new Anotacoes();
                    a.CodigoAnotacoes = int.Parse(leitor["id_anotacao"].ToString());
                    a.IdPessoa = int.Parse(leitor["fk_id_pessoa"].ToString());
                    a.Titulo = leitor["titulo"].ToString();
                    a.Descricao = leitor["descricao"].ToString();
                    a.DataHora = leitor["dataHora"].ToString();

                    list.Add(a);
                }

            }
            catch (Exception e)
            {
                list = new List<Anotacoes>();
            }

            if (con.State == ConnectionState.Open)
                con.Close();

            return list;
        }

        public static Anotacoes BuscarAnotacoes(int idpessoa)
        {
            Anotacoes a = new Anotacoes();
            SqlConnection con =
            new SqlConnection(ConfigurationManager.ConnectionStrings["BCD"].ConnectionString);
            try
            {
                con.Open();
                SqlCommand query = new SqlCommand("SELECT * from Anotacoes WHERE  fk_id_pessoa = @id_pessoa", con);
                query.Parameters.AddWithValue("@idpessoa", idpessoa);
                SqlDataReader leitor = query.ExecuteReader();

                while (leitor.Read())
                {
                    a.CodigoAnotacoes = int.Parse(leitor["id_anotacao"].ToString());
                    a.IdPessoa = int.Parse(leitor["fk_id_pessoa"].ToString());
                    a.Titulo = leitor["titulo"].ToString();
                    a.Descricao = leitor["descricao"].ToString();
                    a.DataHora = leitor["dataHora"].ToString();
                }
            }
            catch (Exception e)
            {
                a = null;
            }

            if (con.State == System.Data.ConnectionState.Open)
                con.Close();

            return a; //Retorna 
        }
    }
}

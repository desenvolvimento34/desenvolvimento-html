﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace WebApplication.Models
{
    public class Dashboard
    {
        //Variáveis
        private double mv_valvula, sp_pressao, pv_pressao, pv_temperatura;

        public double MV_Valvula //Método
        {
            get //Pegar dados privados
            {
                return mv_valvula;
            }
            set //Editar dados privados
            {
                mv_valvula = value;
            }
        }

        public double SP_Pressao //Método
        {
            get //Pegar dados privados 
            {
                return sp_pressao;
            }
            set //Editar dados privados
            {
                sp_pressao = value;
            }
        }

        public double PV_Pressao //Método
        {
            get //Pegar dados privados
            {
                return pv_pressao;
            }
            set//Editar dados privados
            {
                pv_pressao = value;
            }
        }

        public double PV_Temperatura //Método
        {
            get //Pegar dados privados
            {
                return pv_temperatura;
            }
            set //Editar dados privados
            {
                pv_temperatura = value;
            }
        }

        public bool Mostrar()
        {
            bool dash = false; // valor boleano
            SqlConnection con =
                 new SqlConnection(ConfigurationManager.ConnectionStrings["BCD"].ConnectionString);
            try
            {
                con.Open();
                SqlCommand query =
                    new SqlCommand("SELECT * FROM Anotacoes WHERE mv_valvula = @mv_valvula, SP_Pressao = @SP_Pressao, PV_Pressao = @PV_Pressao, PV_Temperatura = @PV_Temperatura ;", con);
                query.Parameters.AddWithValue("@mv_valvula", mv_valvula); //Atributo mv_valvula
                query.Parameters.AddWithValue("@SP_Pressao", SP_Pressao); //Atributo sp_pressao
                query.Parameters.AddWithValue("@PV_Pressao", PV_Pressao); //Atributo pv_pressao
                query.Parameters.AddWithValue("@PV_Temperatura", PV_Temperatura); //Atributo pv_temperatura
                SqlDataReader leitor = query.ExecuteReader();

            }
            catch (Exception e)
            {

                dash = false;

            }

            if (con.State == System.Data.ConnectionState.Open)
                con.Close();

            return dash; //Retorna váriavel login
        }
    }
}
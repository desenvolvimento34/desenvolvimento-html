﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace WebApplication.Models
{
    //Criando ponto no plano cartesiano

    [DataContract] //Veiculando com o formato do JSO
    public class DataPoint //Classe do ponto
    {
         public DataPoint(double x, double y)
        {
            this.X = x;
            this.Y = y;
        }

   

        //Definindo  o nome a ser usado dutante a serialização para JSON
        [DataMember(Name = "x")]
        public Nullable<double> X = null;

        //Definindo o nome a ser usado durante a serialização para JSON
        [DataMember(Name = "y")]
        public Nullable<double> Y = null;
    }
}